class ItemModel:

    def __init__(self, description, quantite):
        self.description = description
        self.quantite = quantite

    def valider(self):
        messages = []
        if not self.description:
            messages.append("La description ne doit pas être vide")
            return messages
        return messages
