import bd 


class UtilisateurModel:

    def __init__(self, identifiant, mot_de_passe):
        self.identifiant = identifiant
        self.mot_de_passe = mot_de_passe

    def valider_creation(self):
        try:
            messages = []
            if not self.identifiant or not self.mot_de_passe:
                messages.append("L'identifiant et le mot de passe ne doivent pas être vides")
                return messages
            connexion = bd.obtenir_connexion()
            curseur = connexion.cursor()
            curseur.execute('SELECT * FROM utilisateurs WHERE identifiant = %s', (self.identifiant,))
            if curseur.fetchone() is not None:
                messages.append("Un utilisateur existe déjà avec cet identifiant")
            return messages
        except Exception as e:
            pass  # todo journalisation
        finally:
            connexion.close()

    def valider_authentification(self):
        try:
            """todo code en pratiquement identique à la méthode valider_création. Réusinage à prévoir"""
            messages = []
            if not self.identifiant or not self.mot_de_passe:
                messages.append("L'identifiant et le mot de passe ne doivent pas être vides")
                return messages
            connexion = bd.obtenir_connexion()
            curseur = connexion.cursor()
            curseur.execute('SELECT * FROM utilisateurs WHERE identifiant = %s and mot_de_passe = %s',
                           (self.identifiant, self.mot_de_passe))
            if curseur.fetchone() is None:
                messages.append("Combinaison identifiant/mot de passe invalide")
            return messages
        except Exception as e:
            pass  # todo journalisation
        finally:
            connexion.close()

    def enregistrer(self):
        try:
            connexion = bd.obtenir_connexion()
            curseur = connexion.cursor()
            curseur.execute('INSERT INTO utilisateurs VALUEs (%s, %s, 0)', (self.identifiant, self.mot_de_passe))
            connexion.commit()
        except Exception as e:
            pass  # todo journalisation
        finally:
            connexion.close()
