from models.utilisateur_model import UtilisateurModel
from flask import (
    Flask,
    render_template,
    url_for,
    request,
    redirect,
    session,
    flash
)

app = Flask(__name__)
app.secret_key = "gb[>WZy.pR9*I*F"


@app.route('/')
def accueil():
    # cookie rebonjour!
    return render_template('accueil.html')


@app.route('/utilisateurs/creer-compte')
def creation_compte():
    return render_template('/utilisateurs/creer.html')


@app.route('/utilisateurs/creer-compte', methods=['POST'])
def creation_compte_post():
    identifiant = request.form.get('id')
    mot_de_passe = request.form.get('mot-passe')
    model = UtilisateurModel(identifiant, mot_de_passe)
    messages = model.valider_creation()
    if len(messages) > 0:
        return render_template('/utilisateurs/creer.html', messages=messages)
    model.enregistrer()
    session["utilisateur"] = model.identifiant
    return redirect(url_for('accueil'))


@app.route("/authentification/connexion")
def authentification():
    return render_template("authentification/authentification.html")


@app.route("/authentification/connexion", methods=["POST"])
def authentification_post():
    identifiant = request.form.get('id')
    mot_de_passe = request.form.get('mot-passe')
    model = UtilisateurModel(identifiant, mot_de_passe)
    messages = model.valider_authentification()
    if messages:
        for message in messages:
            flash(message)
        return render_template("authentification/authentification.html")
    session["utilisateur"] = model.identifiant
    return redirect(url_for("liste"))


@app.route("/authentification/deconnexion")
def deconnexion():
    flash('Vous vous êtes déconecté')
    session.pop('utilisateur', None)
    return redirect(url_for("authentification"))


@app.route('/liste')
def liste():
    # todo
    return render_template("items/liste.html")


@app.route('/items/ajouter')
def creer_item():
    return render_template("items/creer.html")


@app.route('/items/ajouter', methods=['POST'])
def creer_item_post():
    # todo
    pass

@app.route('/items/effacer')
def effacer_item():
    # todo
    pass


if __name__ == '__main__':
    app.run(debug=True)
